
import 'package:flutter/material.dart';
import 'package:hexcolor/hexcolor.dart';

const cntMediumTextStyle=TextStyle(
  fontSize: 18.0,
  color: Colors.white,
);
const cntActiveColor=Colors.green;
const cntInactiveColor=Colors.black38;

enum Gender {
  Male,
  Female,
  Other
}