import 'dart:math';

class BMICalculation {
  int? _height;
  int? _weight;
  double? _bmiResult;

  BMICalculation(this._height, this._weight);

  String Calculation() {
    _bmiResult = _weight! / pow(_height! / 100, 2);
    return _bmiResult!.toStringAsFixed(2);
  }

  String Result() {
    if (_bmiResult! <= 18.49) {
      return "UnderWeight";
    } else if (_bmiResult! >= 18.50 && _bmiResult! <= 24.99) {
      return "NormalWeight";
    } else {
      return "OverWeight";
    }
  }

  String Message() {
    if (_bmiResult! <= 18.49) {
      return "You have a lower than normal body weight!";
    } else if (_bmiResult! >= 18.50 && _bmiResult! <= 24.99) {
      return "You have a normal body";
    } else {
      return "You have a over weight than normal body";
    }
  }
}

enum BMIStatus { UnderWeight, NormalWeight, OverWeight }
