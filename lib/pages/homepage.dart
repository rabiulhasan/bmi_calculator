import 'package:bmi_calculator/constants/constants.dart';
import 'package:bmi_calculator/main.dart';
import 'package:bmi_calculator/models/bmi_calculator.dart';
import 'package:bmi_calculator/pages/resultpage.dart';
import 'package:bmi_calculator/widgets/btn_icon_round.dart';
import 'package:bmi_calculator/widgets/card_gender.dart';
import 'package:bmi_calculator/widgets/custom_button.dart';
import 'package:bmi_calculator/widgets/reuseable_card.dart';
import 'package:flutter/material.dart';


class HomePage extends StatefulWidget {
  const HomePage({Key? key}) : super(key: key);

  @override
  State<HomePage> createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  Color maleColor = cntInactiveColor;
  Color femaleColor = cntInactiveColor;

  Gender? selectedGender;
  int height = 180;
  int weight = 60;
  int age = 20;

//   void  UpdateColor(Gender gender){
//
//     if(gender==Gender.Male)
//       {
//         setState((){
//           maleColor=cntActiveColor;
//           femaleColor=cntInactiveColor;
//         });
//
//       }
//     else if(gender==Gender.Female){
//       setState((){
//         femaleColor=cntActiveColor;
//         maleColor=cntInactiveColor;
//       });
//     }else{
//       setState((){
//          maleColor=cntInactiveColor;
//          femaleColor=cntInactiveColor;
//       });
//     }
//
// }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('BMI Calculator'),
        centerTitle: true,
        leading: Icon(Icons.menu_sharp),
      ),
      body: Column(
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: [
          //Gender Section
          Expanded(
              child: Row(
            children: [
              Expanded(
                child: ReuseableCard(
                  onPressed: () {
                    setState(() {
                      selectedGender = Gender.Male;
                    });
                    // UpdateColor(Gender.Male);
                  },
                  //ternarry operator
                  color: selectedGender == Gender.Male
                      ? cntActiveColor
                      : cntInactiveColor,
                  childCard: CardGender(
                    icon: Icons.male,
                    gender: 'Male'.toUpperCase(),
                  ),
                ),
              ),
              Expanded(
                child: ReuseableCard(
                  onPressed: () {
                    setState(() {
                      selectedGender = Gender.Female;
                    });
                    // UpdateColor(Gender.Female);
                  },
                  color: selectedGender == Gender.Female
                      ? cntActiveColor
                      : cntInactiveColor,
                  childCard: CardGender(
                    icon: Icons.female,
                    gender: 'Female'.toUpperCase(),
                  ),
                ),
              )
            ],
          )),
          //Height Section
          Expanded(
            child: ReuseableCard(
              color: cntInactiveColor,
              onPressed: () {},
              childCard: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Text('Height'.toUpperCase(), style: cntMediumTextStyle),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.baseline,
                    textBaseline: TextBaseline.alphabetic,
                    children: [
                      Text(
                        height.toString(),
                        style: cntMediumTextStyle.copyWith(
                          fontSize: 50,
                        ),
                      ),
                      Text(
                        'cm',
                        style: cntMediumTextStyle,
                      ),
                    ],
                  ),
                  Slider(
                      min: 100.0,
                      max: 200.0,
                      value: height.toDouble(),
                      onChanged: (double newValue) {
                        print(newValue);
                        setState(() {
                          height = newValue.toInt();
                        });
                      })
                ],
              ),
            ),
          ),
          Expanded(
            child: Row(
              children: [
                Expanded(
                  child: ReuseableCard(
                    onPressed: () {},
                    color: cntInactiveColor,
                    childCard: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Text(
                          'Weight'.toUpperCase(),
                          style: cntMediumTextStyle,
                        ),
                        Text(
                          weight.toString(),
                          style: cntMediumTextStyle.copyWith(
                            fontSize: 50,
                            fontWeight: FontWeight.bold,
                          ),
                        ),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            ButtonRoundIcon(
                              onPressed: () {
                                setState(() {
                                  weight--;
                                });
                              },
                              icon: Icons.arrow_back,
                            ),
                            SizedBox(
                              width: 10,
                            ),
                            ButtonRoundIcon(
                              onPressed: () {
                                setState(() {
                                  weight++;
                                });
                              },
                              icon: Icons.arrow_forward,
                            ),
                          ],
                        )
                      ],
                    ),
                  ),
                ),
                Expanded(
                  child: ReuseableCard(
                    onPressed: () {},
                    color: cntInactiveColor,
                    childCard: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Text(
                          'Age'.toUpperCase(),
                          style: cntMediumTextStyle,
                        ),
                        Text(
                          age.toString(),
                          style: cntMediumTextStyle.copyWith(
                            fontWeight: FontWeight.bold,
                            fontSize: 50,
                          ),
                        ),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            ButtonRoundIcon(
                              onPressed: () {
                                setState(() {
                                  age--;
                                });
                              },
                              icon: Icons.arrow_back,
                            ),
                            SizedBox(
                              width: 10,
                            ),
                            ButtonRoundIcon(
                              onPressed: () {
                                setState(() {
                                  age++;
                                });
                              },
                              icon: Icons.arrow_forward,
                            ),
                          ],
                        )
                      ],
                    ),
                  ),
                ),
              ],
            ),
          ),
          CustomButton(
            tittle: 'Calculate your BMI'.toUpperCase(),
            onPressed: () async{
             BMICalculation bmical= BMICalculation(height, weight);

           String status= await Navigator.push(context, MaterialPageRoute(builder: (context){
              return ResultPage(
                bmiResult: bmical.Calculation(),
                result: bmical.Result(),
                message: bmical.Message(),

              );
            }),);
           if(status=="isBack")
             {
               setState((){
                 weight=60;
                 height=180;
                 age=20;
               });

             }
            },

          ),
        ],
      ),
    );
  }
}


