import 'package:bmi_calculator/constants/constants.dart';
import 'package:bmi_calculator/widgets/custom_button.dart';
import 'package:bmi_calculator/widgets/reuseable_card.dart';
import 'package:flutter/material.dart';

class ResultPage extends StatelessWidget {
  final String? bmiResult;
  final String? result;
  final String? message;
  const ResultPage({Key? key,this.bmiResult,this.result,this.message}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text('BMI RESULT'),
        ),
        body: ReuseableCard(
          color: cntActiveColor,
          onPressed: () {},
          childCard: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.stretch,
            children: [
              Center(
                  child: Text(
                'Normal',
                style: cntMediumTextStyle.copyWith(fontSize: 30),
              )),
              Center(
                child: Text(
                  bmiResult.toString(),
                  style: cntMediumTextStyle.copyWith(
                      fontSize: 50, fontWeight: FontWeight.bold),
                ),
              ),
              Center(
                child: Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Text(result.toString(),
                      style: cntMediumTextStyle.copyWith(
                        fontSize: 25,
                      )),
                ),
              ),
              SizedBox(height: 20,),
              Center(
                child: Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Text(message.toString(),
                      style: cntMediumTextStyle.copyWith(
                        fontSize: 25,
                      )),
                ),
              ),
              SizedBox(height: 20,),
              Padding(
                padding: const EdgeInsets.all(15.0),
                child: CustomButton(
                  tittle: 'Re-Calculate',
                  onPressed: (){
                    Navigator.pop(context,'isBack');
                  },
                ),
              ),
            ],
          ),
        ));
  }
}
