import 'package:flutter/material.dart';

class ButtonRoundIcon extends StatelessWidget {
  final Function()? onPressed;
  final IconData? icon;

  const ButtonRoundIcon({Key? key, this.icon, this.onPressed})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return RawMaterialButton(
      onPressed: onPressed,
      constraints: BoxConstraints.tightFor(
        height: 50,
        width: 50,
      ),
      shape: CircleBorder(),
      fillColor: Colors.amber,
      child: Icon(icon),
    );
  }
}
