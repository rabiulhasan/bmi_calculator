import 'package:flutter/material.dart';

class CardGender extends StatelessWidget {
  final IconData? icon;
  final String? gender;

  const CardGender({Key? key, this.icon, this.gender}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        Icon(icon,size: 70,),
        Text(gender!,style: TextStyle(fontSize: 25),),
      ],
    );
  }
}
