import 'package:flutter/material.dart';

class ReuseableCard extends StatelessWidget {
  // const ReuseableCard({
  //   Key? key,
  // }) : super(key: key);
  final Color? color;
  final Widget? childCard;
  final Function()? onPressed;
  ReuseableCard({this.color, this.childCard,this.onPressed});

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap:onPressed,
      child: Container(
        margin: EdgeInsets.all(15),
        decoration: BoxDecoration(
          color: color,
          borderRadius: BorderRadius.circular(10),
        ),
        child: childCard,
      ),
    );
  }
}
